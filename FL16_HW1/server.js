const express = require('express');
const connectDB = require('./config/db');

const app = express();
app.use(express.json({ extended: false }))

// Connect Database
connectDB();

app.get('/', (req, res) => res.send('API Running'));
// Define Routes
app.use('/api/files', require('./routes/api/createFile'));

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => console.log(`Server started on port: ${PORT}`));