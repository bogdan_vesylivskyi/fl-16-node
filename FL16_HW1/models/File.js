const mongoose = require('mongoose');

const FileSchema = new mongoose.Schema({
    message: {
        type: String, // ex. "Success"
    },
    filename: {
        type: String, // ex."notes.txt"
        required: true
    },
    content: {
        type: String, // ex. "1. Create PR for my mentor"
    },
    extension: {
        type: String, // ex. "txt"
    },
    uploadedDate: {
        type: Date,
        default: Date.now
    }
})

module.exports = File = mongoose.model('file', FileSchema);