const express = require('express');
const router = express.Router();
const {check, validationResult} = require('express-validator');

const File = require("../../models/File")

// @route    POST api/files
// @desc     Create file
// @access   Public

router.post('/', [
    check('filename')
        // Here we check that file input is required
        .not().isEmpty().withMessage("Filename is required")
        // Here we check that file input is with valid extention
        .custom((value, {req}) => {
            if (value && !value.match(/\.(log|txt|json|yaml|xml|js)$/i)) throw new Error("Filename should be only with log, txt, json, yaml, xml, js extention");
            return true;
        }),
    check('content').not().isEmpty().withMessage("Please specify 'content' parameter")
], async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()})
    }

    const {filename, content} = req.body;

    try {
        // See if file with the same name exists
        let file = await File.findOne({filename});

        if (file) {
            return res
                .status(400)
                .json({errors: [{message: 'Filename already exists'}]});
        }

        file = new File({
            filename,
            content,
            message: "Success",
            extension: filename.substring(filename.lastIndexOf(".") + 1)
        });

        const createdFile = await file.save();


        // res.status(200).send("File created successfully").json(createdFile);
        res.status(200).send("File created successfully");

    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'});
    }
})

// @route    GET api/files
// @desc     Get all files
// @access   Public

router.get('/', async (req, res) => {
    try {
        // const allFiles = await File.find().populate({path: 'filename'}, ['filename']);
        const allFiles = await File.find();
        if (!allFiles.length) {
            return res.status(400).json({message: 'Client error'})
        }

        console.log(allFiles, "allFiles")
        const allFilesNames = allFiles.map(file => file.filename)

        res.json({"message": "Success", files: allFilesNames});
    } catch (err) {
        console.error(err.message);
        res.status(500).json({message: 'Server Error'})
    }
})

// @route    GET /api/files/:filename
// @desc     Get post by Filename
// @access   Public

router.get('/:filename', async (req, res) => {
    try {
        const files = await File.find({filename: req.params.filename}).limit(1);

        if (!files) {
            return res.status(400).json({message: `No file with ${req.params.filename} filename found`})
        }
        const result = files.map(file => ({
            message: file.message,
            filename: file.filename,
            content: file.content,
            extension: file.extension,
            uploadedDate: file.uploadedDate
        }))[0]

        res.json(result);
    } catch (err) {
        if (err.kind === 'ObjectId') {
            return res.status(404).json({message: `No file with ${req.params.filename} filename found`})
        }
        res.status(500).json({message: 'Server Error'});
    }
})

// @route    DELETE /api/files/:filename
// @desc     Delete post by Filename
// @access   Public

router.delete('/:filename', async (req, res) => {
    try {
        const files = await File.findOneAndRemove({filename: req.params.filename});

        if (!files) {
            return res.status(400).json({message: `No file with ${req.params.filename} filename found`})
        }

        res.json({ message: 'File deleted' });
    } catch (err) {
        if (err.kind === 'ObjectId') {
            return res.status(404).json({message: `No file with ${req.params.filename} filename found`})
        }
        res.status(500).json({message: 'Server Error'});
    }
})


module.exports = router;


