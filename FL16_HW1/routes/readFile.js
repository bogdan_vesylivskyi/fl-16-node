const express = require('express');
const fs = require('fs');
const router = express.Router();

router.get('/', async (req, res) => {
    try {
        await fs.readFile('./files/appendFile.txt', 'utf8', (err, data) => {
            if (err) {
                console.log("Error read file", err);
            } else {
                console.log("**File read successful. File Data**", data)
                res.status(200).send('***File read successful***')
            }
        });
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error :(')
    }
})

module.exports = router;